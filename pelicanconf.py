#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'Michał'
SITENAME = 'sockets\u00b2'
SITEURL = 'https://sockets2.net'
#SITEURL = ''

PATH = 'content'
STATIC_PATHS = ['images']
EXTRA_PATH_METADATA = {
    'images/favicon.ico': {'path': 'favicon.ico'},
}

TIMEZONE = 'Europe/Warsaw'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = ()

# Social widget
SOCIAL = (('linkedin', 'https://www.linkedin.com/in/mwolczyn/'),
          ('email', 'mailto:michal@socks2.net'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# Theme settings
THEME = './themes/pelican-hyde'
BIO = 'Cars, programming and other financially suboptimal hobbies.'
PROFILE_IMAGE = 'socketsocket768_bg.png'
FOOTER_TEXT = 'Copyright \u00a9 2023 Michał Wolczyński'
COLOR_THEME = '00'

# URL settings
ARTICLE_URL = 'posts/{slug}.html'
ARTICLE_SAVE_AS = 'posts/{slug}.html'
DRAFT_URL = 'drafts/posts/{slug}.html'
DRAFT_SAVE_AS = 'drafts/posts/{slug}.html'
PAGES_URL = 'pages/{slug}.html'
PAGES_SAVE_AS = 'pages/{slug}.html'
DRAFT_PAGE_URL = 'drafts/pages/{slug}.html'
DRAFT_PAGE_SAVE_AS = 'drafts/pages/{slug}.html'
TAG_URL = 'tag/{slug}.html'
TAG_SAVE_AS = 'tag/{slug}.html'

